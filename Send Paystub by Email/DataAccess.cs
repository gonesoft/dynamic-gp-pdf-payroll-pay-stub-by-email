﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dexterity;
using Microsoft.Dexterity.Bridge;
using Microsoft.Dexterity.Applications;
using Microsoft.Dexterity.Applications.DynamicsDictionary;
using Send_Paystub_by_Email.Properties;

namespace Send_Paystub_by_Email
{
    class DataAccess
    {
        /// <summary>
        ///  Get all history checked employees by auditcode range only and fill the listview
        /// </summary>
        /// <param name="auditCodeFrom"></param>
        /// <param name="auditCodeTo"></param>
        /// 
        public static void GetAllEmployeesByAuditCodeRange(string auditCodeFrom, string auditCodeTo)
        {
            TableError err;

            //Create a reference to the tables Payroll History Table and Internet Address Table
            UprCheckHistTable employeeList = Dynamics.Tables.UprCheckHist;
            CoINetAddrsTable employeeEmailAddress = Dynamics.Tables.CoINetAddrs;

            //Key 1 look for Audit Control Code, table UPR30100
            employeeList.Key = 1;
            employeeList.Clear();
            employeeList.AuditControlCode.Value = auditCodeFrom;
            employeeList.RangeStart();

            employeeList.Fill();
            employeeList.AuditControlCode.Value = auditCodeTo;
            employeeList.RangeEnd();

            //Key 1 or coINetAddrsIdxID look for master type, master id and address code, table SY01200
            employeeEmailAddress.Key = 1;

            err = employeeList.GetFirst();

            //Loop throught each employee to get default email address due a lack of internal function to extract employee email address
            while (err == TableError.NoError)
            {
                employeeEmailAddress.Clear();
                employeeEmailAddress.MasterType.Value = "EMP"; //Hardcoded because always look for Employee's address
                employeeEmailAddress.MasterId.Value = employeeList.EmployeeId.Value;
                employeeEmailAddress.AddressCode.Value = Dynamics.Functions.GetEmployeePrimaryAddressCode.Invoke(employeeList.EmployeeId.Value);
                employeeEmailAddress.Get();
                
                //Fill up the listview direct 
                GPAddIn.PaystubByEmailForm.listViewEmployees.Items.Add(new EmployeeCollection(
                    employeeList.EmployeeId.Value.TrimEnd(),
                    employeeList.EmployeeName.Value.TrimEnd(),
                    employeeList.CheckDate.Value.ToString("dd/MM/yyyy").TrimEnd(),
                    employeeList.AuditControlCode.Value.TrimEnd(),
                    employeeEmailAddress.INet1.Value.TrimEnd(),
                    ""
                    ));
                
                err = employeeList.GetNext();
            }
            employeeEmailAddress.Close();
            employeeList.Close();
        }

       

        /// <summary>
        ///  Get employees by auditcode range and employee id only
        /// </summary>
        /// <param name="auditCodeFrom"></param>
        /// <param name="auditCodeTo"></param>
        /// <param name="employeeId"></param>
        /// 
        public static void GetAllEmployeesByAuditCodeRangeAndEmployeeId(string auditCodeFrom, string auditCodeTo, string employeeId)
        {
            TableError err;

            //Create a reference to the tables Payroll History Table and Internet Address Table
            UprCheckHistTable employeeList1 = Dynamics.Tables.UprCheckHist;
            CoINetAddrsTable employeeEmailAddress1 = Dynamics.Tables.CoINetAddrs;

            //Key 7 look for Audit Control Code and Employee ID, table UPR30100
            employeeList1.Key = 7;
            employeeList1.Clear();
            employeeList1.EmployeeId.Value = employeeId;
            employeeList1.AuditControlCode.Value = auditCodeFrom;
            employeeList1.RangeStart();

            //employeeList.Fill();
            employeeList1.Clear();
            employeeList1.EmployeeId.Value = employeeId;
            employeeList1.AuditControlCode.Value = auditCodeTo;
            employeeList1.RangeEnd();

            //Key 1 or coINetAddrsIdxID look for master type, master id and address code, table SY01200
            employeeEmailAddress1.Key = 1;

            err = employeeList1.GetFirst();

            //Loop throught each employee to get default email address due a lack of internal function to extract employee email address
            while (err == TableError.NoError)
            {
                employeeEmailAddress1.Clear();
                employeeEmailAddress1.MasterType.Value = "EMP"; //Hardcoded because always look for Employee's address
                employeeEmailAddress1.MasterId.Value = employeeList1.EmployeeId.Value;
                employeeEmailAddress1.AddressCode.Value = Dynamics.Functions.GetEmployeePrimaryAddressCode.Invoke(employeeList1.EmployeeId.Value);
                employeeEmailAddress1.Get();

                //Fill up the listview direct 
                GPAddIn.PaystubByEmailForm.listViewEmployees.Items.Add(new EmployeeCollection(
                    employeeList1.EmployeeId.Value.TrimEnd(),
                    employeeList1.EmployeeName.Value.TrimEnd(),
                    employeeList1.CheckDate.Value.ToString("dd/MM/yyyy").TrimEnd(),
                    employeeList1.AuditControlCode.Value.TrimEnd(),
                    employeeEmailAddress1.INet1.Value.TrimEnd(),
                    ""
                    ));

                err = employeeList1.GetNext();
            }
            employeeEmailAddress1.Close();
            employeeList1.Close();
        }


        /// <summary>
        ///  Get employees by employee id range
        /// </summary>
        /// <param name="auditCodeFrom"></param>
        /// <param name="auditCodeTo"></param>
        /// <param name="employeeId"></param>
        /// 
        public static void GetAllEmployeesByEmployeeIdRange(string employeeIdFrom, string employeeIdTo)
        {
            TableError err;

            //Create a reference to the tables Payroll History Table and Internet Address Table
            UprCheckHistTable employeeList2 = Dynamics.Tables.UprCheckHist;
            CoINetAddrsTable employeeEmailAddress1 = Dynamics.Tables.CoINetAddrs;

            //Key 5 look for  Employee ID, table UPR30100
            employeeList2.Key = 5;
            employeeList2.Clear();
            employeeList2.EmployeeId.Value = employeeIdFrom;
            employeeList2.RangeStart();

            //employeeList1.Fill();
            employeeList2.Clear();
            employeeList2.EmployeeId.Value = employeeIdTo;
            employeeList2.RangeEnd();

            //Key 1 or coINetAddrsIdxID look for master type, master id and address code, table SY01200
            employeeEmailAddress1.Key = 1;

            err = employeeList2.GetFirst();

            //Loop throught each employee to get default email address due a lack of internal function to extract employee email address
            while (err == TableError.NoError)
            {
                employeeEmailAddress1.Clear();
                employeeEmailAddress1.MasterType.Value = "EMP"; //Hardcoded because always look for Employee's address
                employeeEmailAddress1.MasterId.Value = employeeList2.EmployeeId.Value;
                employeeEmailAddress1.AddressCode.Value = Dynamics.Functions.GetEmployeePrimaryAddressCode.Invoke(employeeList2.EmployeeId.Value);
                employeeEmailAddress1.Get();

                //Fill up the listview direct 
                GPAddIn.PaystubByEmailForm.listViewEmployees.Items.Add(new EmployeeCollection(
                    employeeList2.EmployeeId.Value.TrimEnd(),
                    employeeList2.EmployeeName.Value.TrimEnd(),
                    employeeList2.CheckDate.Value.ToString("dd/MM/yyyy").TrimEnd(),
                    employeeList2.AuditControlCode.Value.TrimEnd(),
                    employeeEmailAddress1.INet1.Value.TrimEnd(),
                    ""
                    ));

                err = employeeList2.GetNext();
            }
            employeeEmailAddress1.Close();
            employeeList2.Close();
        }

        /// <summary>
        /// Get the data information of employee's paystub formated in html string
        /// </summary>
        /// <param name="auditCode"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public static string GetEmployeePayStubHtmlData(string auditCode, string employeeId)
        {
            //Reference to temp variables to handle loop codes, descriptions, amounts
            string payCode = string.Empty;
            string payDescription = string.Empty;
            string payRate = string.Empty;
            string payHours = string.Empty;
            string payAmounts = string.Empty;
            string dedCode = string.Empty;
            string dedDescription = string.Empty;
            string dedAmount = string.Empty;
            string taxCode = string.Empty;
            string taxDescription = string.Empty;
            string taxWithheld = string.Empty;
            string benfCode = string.Empty;
            string benfDescription = string.Empty;
            string benfAmounts = string.Empty;

            //Create a reference to Transaction History Table Header of Payroll (UPR30100)
            UprCheckHistTable EmployeeTransactionHistheader = Dynamics.Tables.UprCheckHist;

            //Create a reference to Transaction History Table of Payroll (UPR30300)
            UprTransactionHistTable EmployeeTransactionHist = Dynamics.Tables.UprTransactionHist;

            //Referece to transaction history header to fill year to date colums in paystub (UPR30301) not use it for now.
            //UprTransactionHistHdrTable EmployeeTransactionHistHrd = Dynamics.Tables.UprTransactionHistHdr;

            //Get html file resource into string variable
            String paystub = Resources.HtmlEmployeePaystub;

            //Reference to any error on tables get data
            TableError err;

            //Check transaction header first with key 7, Employee ID and Audit Control Code
            EmployeeTransactionHistheader.Key = 7;
            EmployeeTransactionHistheader.EmployeeId.Value = employeeId;
            EmployeeTransactionHistheader.AuditControlCode.Value = auditCode;
            err = EmployeeTransactionHistheader.Get();

            if (err == TableError.NoError)
            {
                //Formating the header of html template
                paystub = paystub.Replace("{Company Name}", Dynamics.Functions.RwGetCompanyName.Invoke(employeeId));
                paystub = paystub.Replace("{Empoyee Name}", EmployeeTransactionHistheader.EmployeeName.Value);
                paystub = paystub.Replace("{Address line 1}", "");
                paystub = paystub.Replace("{Address line 2}", "");
                paystub = paystub.Replace("{Address line 3}", "");
                paystub = paystub.Replace("{Address line 4}", "");
                paystub = paystub.Replace("{Employee ID}", employeeId);
                paystub = paystub.Replace("{Check Number}", EmployeeTransactionHistheader.CheckNumber.Value.ToString());
                paystub = paystub.Replace("{Check Date}", EmployeeTransactionHistheader.CheckDate.Value.ToString("MM/dd/yyyy").TrimStart());
                paystub = paystub.Replace("{Pay Period Start}", EmployeeTransactionHistheader.PayPeriodBeginDate.Value.ToString("MM/dd/yyyy").TrimStart());
                paystub = paystub.Replace("{Pay Period End}", EmployeeTransactionHistheader.PayPeriodEndDate.Value.ToString("MM/dd/yyyy").TrimStart());

                //Check transactions then with key 7, Employee ID and Audit Control Code
                EmployeeTransactionHist.Key = 7;
                EmployeeTransactionHist.Clear();

                EmployeeTransactionHist.EmployeeId.Value = employeeId;
                EmployeeTransactionHist.AuditControlCode.Value = auditCode;
                EmployeeTransactionHist.RangeStart();


                EmployeeTransactionHist.Clear();
                EmployeeTransactionHist.EmployeeId.Value = employeeId;
                EmployeeTransactionHist.AuditControlCode.Value = auditCode;
                EmployeeTransactionHist.RangeEnd();
                
                err = EmployeeTransactionHist.GetFirst();

                //In this loop every variable will get all info before replace it in html template but, those who don't will call replacement right away.
                while (err == TableError.NoError)
                {
                    switch(EmployeeTransactionHist.PayrollRecordType.Value)
                    {
                        case 1:
                            payCode += "<br />" + EmployeeTransactionHist.PayrollCode.Value;
                            payDescription += "<br />" + Dynamics.Functions.RwGetUprtrxCodeDesc.Invoke(EmployeeTransactionHist.PayrollCode.Value, EmployeeTransactionHist.PayrollRecordType.Value);
                            payRate += "<br />" + EmployeeTransactionHist.PayRate.Value.ToString("C2");
                            payHours += "<br />" + EmployeeTransactionHist.UnitsToPay.Value.ToString("N");
                            payAmounts += "<br />" + EmployeeTransactionHist.UprTrxAmount.Value.ToString("C2");
                            paystub = paystub.Replace("{Total GrPay}", EmployeeTransactionHistheader.GrossWagesPayRun.Value.ToString("C2"));
                            break;
                        case 2:
                            dedCode += "<br />"  + EmployeeTransactionHist.PayrollCode.Value;
                            dedDescription += "<br />" + Dynamics.Functions.RwGetUprtrxCodeDesc.Invoke(EmployeeTransactionHist.PayrollCode.Value, EmployeeTransactionHist.PayrollRecordType.Value);
                            dedAmount += "<br />" + EmployeeTransactionHist.UprTrxAmount.Value.ToString("C2");
                            paystub = paystub.Replace("{Total Ded. A}", EmployeeTransactionHistheader.TotalDeductions.Value.ToString("C2"));
                            break;
                        case 3:
                            benfCode += "<br />" + EmployeeTransactionHist.PayrollCode.Value;
                            benfDescription += "<br />" + Dynamics.Functions.RwGetUprtrxCodeDesc.Invoke(EmployeeTransactionHist.PayrollCode.Value, EmployeeTransactionHist.PayrollRecordType.Value);
                            benfAmounts += "<br />" + EmployeeTransactionHist.UprTrxAmount.Value.ToString("C2");
                            paystub = paystub.Replace("{Total Benfts}", EmployeeTransactionHistheader.TotalBenefits.Value.ToString("C2"));
                            break;
                        case 4:
                            taxCode += "<br />" + EmployeeTransactionHist.PayrollCode.Value;
                            taxDescription += "<br />" + Dynamics.Functions.RwGetUprtrxCodeDesc.Invoke(EmployeeTransactionHist.PayrollCode.Value, EmployeeTransactionHist.PayrollRecordType.Value);
                            taxWithheld += "<br />" + EmployeeTransactionHist.UprTrxAmount.Value.ToString("C2");
                            paystub = paystub.Replace("{Tax Federa}", EmployeeTransactionHistheader.FederalWithholdingPayRun.Value.ToString("C2"));
                            paystub = paystub.Replace("{Tax FICASo}", EmployeeTransactionHistheader.FicaSocialSecurityWithholdingPayRun.Value.ToString("C2"));
                            paystub = paystub.Replace("{Tax FICAMe}", EmployeeTransactionHistheader.FicaMedicareWithholdingPayRun.Value.ToString("C2"));
                            paystub = paystub.Replace("{Total Tax Wd}", EmployeeTransactionHistheader.TotalTaxes.Value.ToString("C2"));
                            paystub = paystub.Replace("{Total Net Pay}", EmployeeTransactionHistheader.NetWagesPayRun.Value.ToString("C2"));
                            break;
                    }
                        
                    err = EmployeeTransactionHist.GetNext();
                }
                EmployeeTransactionHist.Close();
                EmployeeTransactionHistheader.Close();


                //replacement of the rest of the variable in the body
                paystub = paystub.Replace("{Code ID}", payCode);
                paystub = paystub.Replace("{Code Description}", payDescription);
                paystub = paystub.Replace("{Pay Rate}", payRate);
                paystub = paystub.Replace("{Hours}", payHours);
                paystub = paystub.Replace("{Amount}", payAmounts);
                paystub = paystub.Replace("{Ded. Code}", dedCode);
                paystub = paystub.Replace("{Deduction Code Description}", dedDescription);
                paystub = paystub.Replace("{Ded. Amnts}", dedAmount);
                paystub = paystub.Replace("{Tax Code}", taxCode);
                paystub = paystub.Replace("{Tax Code Description}", taxDescription);
                paystub = paystub.Replace("{Tax Withld}", taxWithheld);
                paystub = paystub.Replace("{Benf Code}", benfCode);
                paystub = paystub.Replace("{Benefit Code Description}", benfDescription);
                paystub = paystub.Replace("{Benef Amnts}", benfAmounts);

            }
            else
            {
                EmployeeTransactionHistheader.Close();
                throw new System.ArgumentException("History table level exception: " + err.ToString());
            }
            return paystub;

        }
        
        
    }
    
}
