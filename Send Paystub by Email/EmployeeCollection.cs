﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Send_Paystub_by_Email
{
    public class EmployeeCollection : ListViewItem
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string PayrollDate { get; set; }
        public string PayrollAuditCode { get; set; }
        public string EmailAddress { get; set; }
        public string EmailStatus { get; set; }

        public EmployeeCollection()
        {
 
        }

        /// <summary>
        /// Get all required information to add it in the view list
        /// </summary>
        /// <param name="EmployeeId"></param>
        /// <param name="EmployeeName"></param>
        /// <param name="PayrollDate"></param>
        /// <param name="PayrollAuditCode"></param>
        /// <param name="EmailAddress"></param>
        /// <param name="EmailStatus"></param>
        public EmployeeCollection(string EmployeeId, string EmployeeName, string PayrollDate, string PayrollAuditCode, string EmailAddress, string EmailStatus )
        {
            base.Text = EmployeeId;
            base.SubItems.Add(EmployeeName);
            base.SubItems.Add(PayrollDate);
            base.SubItems.Add(PayrollAuditCode);
            base.SubItems.Add(EmailAddress);
            base.SubItems.Add(EmailStatus);
        }
    }
}
