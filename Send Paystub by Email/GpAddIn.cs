#region Credits
//
// Author:
// Gregory J Sosa (mailto:gsosa@consejosytrucosdynamicsgp.info)
// All rigths reserved 2017
//
// https://consejosytrucosdynamicsgp.info
//
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Dexterity.Bridge;
using Microsoft.Dexterity.Applications;
using Microsoft.Dexterity.Applications.DynamicsDictionary;
using System.Windows.Forms;

namespace Send_Paystub_by_Email
{
    [SupportedDexPlatforms(DexPlatforms.DesktopClient)]
    public class GPAddIn : IDexterityAddIn
    {
        // IDexterityAddIn interface declare objects

        //Reference to Paystub by Email WinForm object
        public static DynamicsGPFormPaystubMain PaystubByEmailForm;

        //Creating a reference to UPR Inquiry Check WinForm
        public static UprInquiryCheckForm CheckFormPaystubByEmail = Dynamics.Forms.UprInquiryCheck;

        //Reference to workflow setup WinForm
        public static WfSetupForm.WorkflowSetupWindow WorkflowSetupForm = Dynamics.Forms.WfSetup.WorkflowSetup;

        //Employee lookup flag to indicate form should be closed
        public static Boolean isEmployeeLookupOpen = false;

        //Payroll Audit Code lookup flag to indicate form should be closed
        public static Boolean isPayrollAuditCodeOpen = false;

        //Location lookup flag to indicate winform open
        public static Boolean isLocationLookupOpen = false;

        //This flag is used to know where (from) or (to) textbox are called, false always is (to)
        public static Boolean isTxtFromFlag = false;

        //Create a reference to the employee payroll lookup
        public static Microsoft.Dexterity.Applications.SmartListDictionary.EmployeeLookupForm 
            employeeLookupObject = SmartList.Forms.EmployeeLookup;

        //Create a reference to the audit code payroll lookup
        public static Microsoft.Dexterity.Applications.SmartListDictionary.UprAuditControlCodeLookupForm
            auditcodeLookupObject = SmartList.Forms.UprAuditControlCodeLookup;


        public void Initialize()
        {
            //Add Paystub by Email Winform to aditional menu in UPR Inquiry Check Window as addin
            CheckFormPaystubByEmail.AddMenuHandler(OpenPaystubByEmail, "Paystub by Email", "");

            //Set the focus in Paystub by Email WinForm before workflow setup closes
            WorkflowSetupForm.CloseBeforeOriginal += WorkflowSetupForm_CloseBeforeOriginal;

            //Set event when user select an employee from lookup window
            employeeLookupObject.EmployeeLookup.SelectButton.ClickBeforeOriginal += SelectButton_ClickBeforeOriginal;

            //Set event when user select an audit code payroll lookup window
            auditcodeLookupObject.UprAuditControlCodeLookup.SelectButton.ClickBeforeOriginal += SelectButton_ClickBeforeOriginal;

        }

        //this event handles the information setup into both textbox 'from' and 'to'
        void SelectButton_ClickBeforeOriginal(object sender, System.ComponentModel.CancelEventArgs e)
        {
            #region Employee Lookup Setup
            if (isEmployeeLookupOpen)
            {
                //by default set both textbox info but set the focus on the last textbox
                if (isTxtFromFlag)
                {
                    PaystubByEmailForm.textboxEmployeeIdFrom.Text = employeeLookupObject.EmployeeLookup.EmployeeLookupScroll.EmployeeId.Value;
                    PaystubByEmailForm.textboxEmployeeIdTo.Text = employeeLookupObject.EmployeeLookup.EmployeeLookupScroll.EmployeeId.Value;
                    
                    //set the focus on the next textbox once information setup into from textbox
                    PaystubByEmailForm.textboxEmployeeIdTo.Focus();
                    PaystubByEmailForm.textboxEmployeeIdTo.SelectAll();

                }
                else
                {
                    PaystubByEmailForm.textboxEmployeeIdTo.Text = employeeLookupObject.EmployeeLookup.EmployeeLookupScroll.EmployeeId.Value;

                    PaystubByEmailForm.textboxEmployeeIdTo.Focus();
                }
                isEmployeeLookupOpen = false;
            }
            #endregion

            #region Auditcode Lookup Setup
            if (isPayrollAuditCodeOpen)
            {
                //by default set both textbox info but set the focus on the last textbox
                //and 'PRIMARY' code for location id will setup
                if (isTxtFromFlag)
                {
                    PaystubByEmailForm.textBoxPayrollDateFrom.Text = auditcodeLookupObject.UprAuditControlCodeLookup.UprAuditControlCodeLookupScroll.AuditControlCode.Value;
                    PaystubByEmailForm.textBoxPayrollDateTo.Text = auditcodeLookupObject.UprAuditControlCodeLookup.UprAuditControlCodeLookupScroll.AuditControlCode.Value;
                   
                    //set the focus on the next textbox once information setup into from textbox
                    PaystubByEmailForm.textBoxPayrollDateTo.Focus();
                    PaystubByEmailForm.textBoxPayrollDateTo.SelectAll();

                    PaystubByEmailForm.buttonAddRefresh_Click(sender, e);
                }
                else
                {
                    PaystubByEmailForm.textBoxPayrollDateTo.Text = auditcodeLookupObject.UprAuditControlCodeLookup.UprAuditControlCodeLookupScroll.AuditControlCode.Value;

                    PaystubByEmailForm.textBoxPayrollDateTo.Focus();
                }
                isPayrollAuditCodeOpen = false;
            }
            #endregion

        }

   

       
        //this event handles the focus before workfow setup winform close
        void WorkflowSetupForm_CloseBeforeOriginal(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PaystubByEmailForm.textBoxPayrollDateFrom.Focus();
        }

       

        //Method to open Paystub by Email WinForm
        private void OpenPaystubByEmail(object sender, EventArgs e)
        {
            
            PaystubByEmailForm = new DynamicsGPFormPaystubMain();
            PaystubByEmailForm.Show();
            PaystubByEmailForm.Activate();

            //set focus on textbox after open and active
            PaystubByEmailForm.textBoxPayrollDateFrom.Focus();

            
        }
    }
}
