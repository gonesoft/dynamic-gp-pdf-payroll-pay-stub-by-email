﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Send_Paystub_by_Email
{
    public class HtmlEmailHelper
    {
        /*
         * Generate needs Auditcode, employee id
         * Send needs all configuration from workflow (needs to be open hidden) and employee email
         * Process needs to get paystub info by employee by id code (this case by row when listed in listview)
         * then setup this info into html template. After that, get email and send this html. Set status result in list view row.
         * repeat the process.
         * */

        public string HostSmtp { get; set; }
        public int Port { get; set; }
        public string EmailSender { get; set; }
        public string EmailPassword { get; set; }
        public bool SSL { get; set; }



        public string GenerateAndSend(HtmlEmailHelper h, string auditCode, string employeeId, string email)
        {
            string status;
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(h.EmailSender);
                mail.To.Add(email);
                mail.Subject = 
                mail.Body = "";
                
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = h.HostSmtp;
                    smtp.EnableSsl = h.SSL;
                    smtp.Port = h.Port;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(h.EmailSender, h.EmailPassword);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.Send(mail);
                }
                mail.Dispose();
                status = "Message sent";
            }
            catch (Exception ex)
            {
                status = ex.ToString();
            }
            
            return status;
        }
    }
}
