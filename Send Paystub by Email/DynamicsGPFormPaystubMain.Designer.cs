﻿namespace Send_Paystub_by_Email
{
    partial class DynamicsGPFormPaystubMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.GroupBox groupBoxSelectBy;
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonEmployeeId = new System.Windows.Forms.RadioButton();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.buttonLookupPayrollDateFrom = new System.Windows.Forms.Button();
            this.buttonLookupPayrollDateTo = new System.Windows.Forms.Button();
            this.labelPayrollFrom = new System.Windows.Forms.Label();
            this.textBoxPayrollDateFrom = new System.Windows.Forms.TextBox();
            this.textBoxPayrollDateTo = new System.Windows.Forms.TextBox();
            this.labelPayrollTo = new System.Windows.Forms.Label();
            this.textboxEmployeeIdFrom = new System.Windows.Forms.TextBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.buttonGenerateSend = new System.Windows.Forms.Button();
            this.buttonEliminateAll = new System.Windows.Forms.Button();
            this.buttonEliminate = new System.Windows.Forms.Button();
            this.buttonAddRefresh = new System.Windows.Forms.Button();
            this.labelEmployeeIdFrom = new System.Windows.Forms.Label();
            this.buttonLookupEmployeeIdFrom = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.buttonCallConfiguration = new System.Windows.Forms.Button();
            this.buttonWorkflowSetup = new System.Windows.Forms.Button();
            this.labelAuditCode = new System.Windows.Forms.Label();
            this.textboxEmployeeIdTo = new System.Windows.Forms.TextBox();
            this.labelEmployeeIdTo = new System.Windows.Forms.Label();
            this.listViewEmployees = new System.Windows.Forms.ListView();
            this.buttonLookupEmployeeIdTo = new System.Windows.Forms.Button();
            groupBoxSelectBy = new System.Windows.Forms.GroupBox();
            groupBoxSelectBy.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSelectBy
            // 
            groupBoxSelectBy.BackColor = System.Drawing.SystemColors.ButtonFace;
            groupBoxSelectBy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            groupBoxSelectBy.Controls.Add(this.label1);
            groupBoxSelectBy.Controls.Add(this.radioButtonEmployeeId);
            groupBoxSelectBy.Controls.Add(this.radioButtonAll);
            groupBoxSelectBy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            groupBoxSelectBy.ForeColor = System.Drawing.Color.Black;
            groupBoxSelectBy.Location = new System.Drawing.Point(333, 30);
            groupBoxSelectBy.Name = "groupBoxSelectBy";
            groupBoxSelectBy.Size = new System.Drawing.Size(208, 27);
            groupBoxSelectBy.TabIndex = 48;
            groupBoxSelectBy.TabStop = false;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 49;
            this.label1.Text = "Select by:";
            // 
            // radioButtonEmployeeId
            // 
            this.radioButtonEmployeeId.AutoSize = true;
            this.radioButtonEmployeeId.Location = new System.Drawing.Point(120, 7);
            this.radioButtonEmployeeId.Name = "radioButtonEmployeeId";
            this.radioButtonEmployeeId.Size = new System.Drawing.Size(82, 17);
            this.radioButtonEmployeeId.TabIndex = 6;
            this.radioButtonEmployeeId.Text = "Employee id";
            this.radioButtonEmployeeId.UseVisualStyleBackColor = true;
            this.radioButtonEmployeeId.CheckedChanged += new System.EventHandler(this.radioButtonEmployeeId_CheckedChanged);
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.AutoSize = true;
            this.radioButtonAll.Checked = true;
            this.radioButtonAll.Location = new System.Drawing.Point(75, 7);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(36, 17);
            this.radioButtonAll.TabIndex = 5;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "All";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            // 
            // buttonLookupPayrollDateFrom
            // 
            this.buttonLookupPayrollDateFrom.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonLookupPayrollDateFrom, Microsoft.Dexterity.Shell.DexButtonType.Field);
            this.buttonLookupPayrollDateFrom.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(173)))), ((int)(((byte)(179)))));
            this.buttonLookupPayrollDateFrom.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupPayrollDateFrom.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupPayrollDateFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLookupPayrollDateFrom.Image = global::Send_Paystub_by_Email.Properties.Resources.Field_Lookup;
            this.buttonLookupPayrollDateFrom.Location = new System.Drawing.Point(313, 36);
            this.buttonLookupPayrollDateFrom.Name = "buttonLookupPayrollDateFrom";
            this.buttonLookupPayrollDateFrom.Size = new System.Drawing.Size(20, 20);
            this.buttonLookupPayrollDateFrom.TabIndex = 2;
            this.buttonLookupPayrollDateFrom.TabStop = false;
            this.buttonLookupPayrollDateFrom.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonLookupPayrollDateFrom.UseVisualStyleBackColor = false;
            this.buttonLookupPayrollDateFrom.Click += new System.EventHandler(this.buttonLookupPayrollDateFrom_Click);
            // 
            // buttonLookupPayrollDateTo
            // 
            this.buttonLookupPayrollDateTo.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonLookupPayrollDateTo, Microsoft.Dexterity.Shell.DexButtonType.Field);
            this.buttonLookupPayrollDateTo.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(173)))), ((int)(((byte)(179)))));
            this.buttonLookupPayrollDateTo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupPayrollDateTo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupPayrollDateTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLookupPayrollDateTo.Image = global::Send_Paystub_by_Email.Properties.Resources.Field_Lookup;
            this.buttonLookupPayrollDateTo.Location = new System.Drawing.Point(313, 55);
            this.buttonLookupPayrollDateTo.Name = "buttonLookupPayrollDateTo";
            this.buttonLookupPayrollDateTo.Size = new System.Drawing.Size(20, 20);
            this.buttonLookupPayrollDateTo.TabIndex = 4;
            this.buttonLookupPayrollDateTo.TabStop = false;
            this.buttonLookupPayrollDateTo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonLookupPayrollDateTo.UseVisualStyleBackColor = false;
            this.buttonLookupPayrollDateTo.Click += new System.EventHandler(this.buttonLookupPayrollDateTo_Click);
            // 
            // labelPayrollFrom
            // 
            this.dexLabelProvider.SetLinkField(this.labelPayrollFrom, "textBoxPayrollDateFrom");
            this.labelPayrollFrom.Location = new System.Drawing.Point(76, 40);
            this.labelPayrollFrom.Name = "labelPayrollFrom";
            this.labelPayrollFrom.Size = new System.Drawing.Size(49, 13);
            this.labelPayrollFrom.TabIndex = 44;
            this.labelPayrollFrom.Text = "From:";
            // 
            // textBoxPayrollDateFrom
            // 
            this.textBoxPayrollDateFrom.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxPayrollDateFrom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPayrollDateFrom.Location = new System.Drawing.Point(131, 40);
            this.textBoxPayrollDateFrom.Name = "textBoxPayrollDateFrom";
            this.textBoxPayrollDateFrom.Size = new System.Drawing.Size(180, 13);
            this.textBoxPayrollDateFrom.TabIndex = 1;
            // 
            // textBoxPayrollDateTo
            // 
            this.textBoxPayrollDateTo.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxPayrollDateTo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPayrollDateTo.Location = new System.Drawing.Point(131, 59);
            this.textBoxPayrollDateTo.Name = "textBoxPayrollDateTo";
            this.textBoxPayrollDateTo.Size = new System.Drawing.Size(180, 13);
            this.textBoxPayrollDateTo.TabIndex = 3;
            // 
            // labelPayrollTo
            // 
            this.dexLabelProvider.SetLinkField(this.labelPayrollTo, "textBoxPayrollDateTo");
            this.labelPayrollTo.Location = new System.Drawing.Point(76, 59);
            this.labelPayrollTo.Name = "labelPayrollTo";
            this.labelPayrollTo.Size = new System.Drawing.Size(35, 13);
            this.labelPayrollTo.TabIndex = 43;
            this.labelPayrollTo.Text = "To:";
            // 
            // textboxEmployeeIdFrom
            // 
            this.textboxEmployeeIdFrom.BackColor = System.Drawing.SystemColors.Window;
            this.textboxEmployeeIdFrom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textboxEmployeeIdFrom.Enabled = false;
            this.textboxEmployeeIdFrom.Location = new System.Drawing.Point(592, 40);
            this.textboxEmployeeIdFrom.Name = "textboxEmployeeIdFrom";
            this.textboxEmployeeIdFrom.Size = new System.Drawing.Size(201, 13);
            this.textboxEmployeeIdFrom.TabIndex = 7;
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAccept.BackColor = System.Drawing.Color.Transparent;
            this.btnAccept.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(173)))), ((int)(((byte)(179)))));
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAccept.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAccept.Location = new System.Drawing.Point(-97, -1);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(98, 24);
            this.btnAccept.TabIndex = 36;
            this.btnAccept.Text = "Aceptar";
            this.btnAccept.UseVisualStyleBackColor = false;
            // 
            // buttonGenerateSend
            // 
            this.buttonGenerateSend.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonGenerateSend.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonGenerateSend, Microsoft.Dexterity.Shell.DexButtonType.ToolbarWithSeparator);
            this.buttonGenerateSend.FlatAppearance.BorderSize = 0;
            this.buttonGenerateSend.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonGenerateSend.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonGenerateSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGenerateSend.Image = global::Send_Paystub_by_Email.Properties.Resources.Toolbar_Post;
            this.buttonGenerateSend.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonGenerateSend.Location = new System.Drawing.Point(295, -1);
            this.buttonGenerateSend.Name = "buttonGenerateSend";
            this.buttonGenerateSend.Size = new System.Drawing.Size(139, 24);
            this.buttonGenerateSend.TabIndex = 12;
            this.buttonGenerateSend.Text = "Generate and Send";
            this.buttonGenerateSend.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonGenerateSend.UseVisualStyleBackColor = false;
            this.buttonGenerateSend.Click += new System.EventHandler(this.buttonGenerateSend_Click);
            // 
            // buttonEliminateAll
            // 
            this.buttonEliminateAll.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonEliminateAll.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonEliminateAll, Microsoft.Dexterity.Shell.DexButtonType.ToolbarWithSeparator);
            this.buttonEliminateAll.FlatAppearance.BorderSize = 0;
            this.buttonEliminateAll.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonEliminateAll.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonEliminateAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminateAll.Image = global::Send_Paystub_by_Email.Properties.Resources.Toolbar_Void;
            this.buttonEliminateAll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEliminateAll.Location = new System.Drawing.Point(95, -1);
            this.buttonEliminateAll.Name = "buttonEliminateAll";
            this.buttonEliminateAll.Size = new System.Drawing.Size(103, 24);
            this.buttonEliminateAll.TabIndex = 15;
            this.buttonEliminateAll.Text = "Delete All";
            this.buttonEliminateAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEliminateAll.UseVisualStyleBackColor = false;
            this.buttonEliminateAll.Click += new System.EventHandler(this.buttonEliminateAll_Click);
            // 
            // buttonEliminate
            // 
            this.buttonEliminate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonEliminate.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonEliminate, Microsoft.Dexterity.Shell.DexButtonType.ToolbarWithSeparator);
            this.buttonEliminate.FlatAppearance.BorderSize = 0;
            this.buttonEliminate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonEliminate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonEliminate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEliminate.Image = global::Send_Paystub_by_Email.Properties.Resources.Toolbar_Delete;
            this.buttonEliminate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonEliminate.Location = new System.Drawing.Point(6, -1);
            this.buttonEliminate.Name = "buttonEliminate";
            this.buttonEliminate.Size = new System.Drawing.Size(77, 24);
            this.buttonEliminate.TabIndex = 14;
            this.buttonEliminate.Text = "Delete";
            this.buttonEliminate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonEliminate.UseVisualStyleBackColor = false;
            this.buttonEliminate.Click += new System.EventHandler(this.buttonEliminate_Click);
            // 
            // buttonAddRefresh
            // 
            this.buttonAddRefresh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonAddRefresh.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonAddRefresh, Microsoft.Dexterity.Shell.DexButtonType.ToolbarWithSeparator);
            this.buttonAddRefresh.FlatAppearance.BorderSize = 0;
            this.buttonAddRefresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonAddRefresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonAddRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddRefresh.Image = global::Send_Paystub_by_Email.Properties.Resources.Toolbar_Redisplay;
            this.buttonAddRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddRefresh.Location = new System.Drawing.Point(204, -1);
            this.buttonAddRefresh.Name = "buttonAddRefresh";
            this.buttonAddRefresh.Size = new System.Drawing.Size(85, 24);
            this.buttonAddRefresh.TabIndex = 11;
            this.buttonAddRefresh.Text = "Refresh";
            this.buttonAddRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddRefresh.UseVisualStyleBackColor = false;
            this.buttonAddRefresh.Click += new System.EventHandler(this.buttonAddRefresh_Click);
            // 
            // labelEmployeeIdFrom
            // 
            this.dexLabelProvider.SetLinkField(this.labelEmployeeIdFrom, "textBoxPayrollDateFrom");
            this.labelEmployeeIdFrom.Location = new System.Drawing.Point(542, 40);
            this.labelEmployeeIdFrom.Name = "labelEmployeeIdFrom";
            this.labelEmployeeIdFrom.Size = new System.Drawing.Size(47, 13);
            this.labelEmployeeIdFrom.TabIndex = 35;
            this.labelEmployeeIdFrom.Text = "From:";
            // 
            // buttonLookupEmployeeIdFrom
            // 
            this.buttonLookupEmployeeIdFrom.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonLookupEmployeeIdFrom, Microsoft.Dexterity.Shell.DexButtonType.Field);
            this.buttonLookupEmployeeIdFrom.Enabled = false;
            this.buttonLookupEmployeeIdFrom.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(173)))), ((int)(((byte)(179)))));
            this.buttonLookupEmployeeIdFrom.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupEmployeeIdFrom.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupEmployeeIdFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLookupEmployeeIdFrom.Image = global::Send_Paystub_by_Email.Properties.Resources.Field_Lookup;
            this.buttonLookupEmployeeIdFrom.Location = new System.Drawing.Point(795, 36);
            this.buttonLookupEmployeeIdFrom.Name = "buttonLookupEmployeeIdFrom";
            this.buttonLookupEmployeeIdFrom.Size = new System.Drawing.Size(20, 20);
            this.buttonLookupEmployeeIdFrom.TabIndex = 8;
            this.buttonLookupEmployeeIdFrom.TabStop = false;
            this.buttonLookupEmployeeIdFrom.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonLookupEmployeeIdFrom.UseVisualStyleBackColor = false;
            this.buttonLookupEmployeeIdFrom.Click += new System.EventHandler(this.buttonLookupEmployeeIdFrom_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.progressBar1.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.progressBar1.Location = new System.Drawing.Point(8, 404);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(912, 19);
            this.progressBar1.TabIndex = 46;
            this.progressBar1.UseWaitCursor = true;
            this.progressBar1.Visible = false;
            // 
            // buttonCallConfiguration
            // 
            this.buttonCallConfiguration.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonCallConfiguration.BackColor = System.Drawing.Color.Transparent;
            this.buttonCallConfiguration.FlatAppearance.BorderSize = 0;
            this.buttonCallConfiguration.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonCallConfiguration.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonCallConfiguration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCallConfiguration.Image = global::Send_Paystub_by_Email.Properties.Resources.Field_Edit;
            this.buttonCallConfiguration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCallConfiguration.Location = new System.Drawing.Point(727, -1);
            this.buttonCallConfiguration.Name = "buttonCallConfiguration";
            this.buttonCallConfiguration.Size = new System.Drawing.Size(167, 24);
            this.buttonCallConfiguration.TabIndex = 42;
            this.buttonCallConfiguration.Text = "Configuración correo saliente";
            this.buttonCallConfiguration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonCallConfiguration.UseVisualStyleBackColor = false;
            // 
            // buttonWorkflowSetup
            // 
            this.buttonWorkflowSetup.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonWorkflowSetup.BackColor = System.Drawing.Color.Transparent;
            this.dexButtonProvider.SetButtonType(this.buttonWorkflowSetup, Microsoft.Dexterity.Shell.DexButtonType.ToolbarWithSeparator);
            this.buttonWorkflowSetup.FlatAppearance.BorderSize = 0;
            this.buttonWorkflowSetup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonWorkflowSetup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonWorkflowSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWorkflowSetup.Image = global::Send_Paystub_by_Email.Properties.Resources.Field_Edit;
            this.buttonWorkflowSetup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonWorkflowSetup.Location = new System.Drawing.Point(806, -1);
            this.buttonWorkflowSetup.Name = "buttonWorkflowSetup";
            this.buttonWorkflowSetup.Size = new System.Drawing.Size(111, 24);
            this.buttonWorkflowSetup.TabIndex = 13;
            this.buttonWorkflowSetup.Text = "Workflow Setup";
            this.buttonWorkflowSetup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonWorkflowSetup.UseVisualStyleBackColor = false;
            this.buttonWorkflowSetup.Click += new System.EventHandler(this.buttonWorkflowSetup_Click);
            // 
            // labelAuditCode
            // 
            this.dexLabelProvider.SetLinkField(this.labelAuditCode, "textBoxPayrollDateFrom");
            this.labelAuditCode.Location = new System.Drawing.Point(12, 39);
            this.labelAuditCode.Name = "labelAuditCode";
            this.labelAuditCode.Size = new System.Drawing.Size(66, 14);
            this.labelAuditCode.TabIndex = 49;
            this.labelAuditCode.Text = "Audit code";
            // 
            // textboxEmployeeIdTo
            // 
            this.textboxEmployeeIdTo.BackColor = System.Drawing.SystemColors.Window;
            this.textboxEmployeeIdTo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textboxEmployeeIdTo.Enabled = false;
            this.textboxEmployeeIdTo.Location = new System.Drawing.Point(592, 59);
            this.textboxEmployeeIdTo.Name = "textboxEmployeeIdTo";
            this.textboxEmployeeIdTo.Size = new System.Drawing.Size(201, 13);
            this.textboxEmployeeIdTo.TabIndex = 9;
            // 
            // labelEmployeeIdTo
            // 
            this.dexLabelProvider.SetLinkField(this.labelEmployeeIdTo, "textboxEmployeeIdTo");
            this.labelEmployeeIdTo.Location = new System.Drawing.Point(542, 59);
            this.labelEmployeeIdTo.Name = "labelEmployeeIdTo";
            this.labelEmployeeIdTo.Size = new System.Drawing.Size(47, 13);
            this.labelEmployeeIdTo.TabIndex = 52;
            this.labelEmployeeIdTo.Text = "To:";
            // 
            // listViewEmployees
            // 
            this.listViewEmployees.AllowColumnReorder = true;
            this.listViewEmployees.CheckBoxes = true;
            this.listViewEmployees.FullRowSelect = true;
            this.listViewEmployees.GridLines = true;
            this.listViewEmployees.HideSelection = false;
            this.listViewEmployees.Location = new System.Drawing.Point(8, 81);
            this.listViewEmployees.Name = "listViewEmployees";
            this.listViewEmployees.Size = new System.Drawing.Size(913, 317);
            this.listViewEmployees.TabIndex = 53;
            this.listViewEmployees.UseCompatibleStateImageBehavior = false;
            // 
            // buttonLookupEmployeeIdTo
            // 
            this.buttonLookupEmployeeIdTo.BackColor = System.Drawing.Color.Transparent;
            this.buttonLookupEmployeeIdTo.Enabled = false;
            this.buttonLookupEmployeeIdTo.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(173)))), ((int)(((byte)(179)))));
            this.buttonLookupEmployeeIdTo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupEmployeeIdTo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.buttonLookupEmployeeIdTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLookupEmployeeIdTo.Image = global::Send_Paystub_by_Email.Properties.Resources.Field_Lookup;
            this.buttonLookupEmployeeIdTo.Location = new System.Drawing.Point(795, 55);
            this.buttonLookupEmployeeIdTo.Name = "buttonLookupEmployeeIdTo";
            this.buttonLookupEmployeeIdTo.Size = new System.Drawing.Size(20, 20);
            this.buttonLookupEmployeeIdTo.TabIndex = 10;
            this.buttonLookupEmployeeIdTo.TabStop = false;
            this.buttonLookupEmployeeIdTo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonLookupEmployeeIdTo.UseVisualStyleBackColor = false;
            this.buttonLookupEmployeeIdTo.Click += new System.EventHandler(this.buttonLookupEmployeeIdTo_Click);
            // 
            // DynamicsGPFormPaystubMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 427);
            this.Controls.Add(this.buttonLookupEmployeeIdTo);
            this.Controls.Add(this.labelEmployeeIdFrom);
            this.Controls.Add(this.buttonLookupEmployeeIdFrom);
            this.Controls.Add(this.listViewEmployees);
            this.Controls.Add(this.textboxEmployeeIdTo);
            this.Controls.Add(this.labelAuditCode);
            this.Controls.Add(this.textboxEmployeeIdFrom);
            this.Controls.Add(this.labelEmployeeIdTo);
            this.Controls.Add(groupBoxSelectBy);
            this.Controls.Add(this.buttonWorkflowSetup);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonLookupPayrollDateFrom);
            this.Controls.Add(this.buttonLookupPayrollDateTo);
            this.Controls.Add(this.labelPayrollFrom);
            this.Controls.Add(this.textBoxPayrollDateFrom);
            this.Controls.Add(this.textBoxPayrollDateTo);
            this.Controls.Add(this.labelPayrollTo);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.buttonGenerateSend);
            this.Controls.Add(this.buttonEliminateAll);
            this.Controls.Add(this.buttonEliminate);
            this.Controls.Add(this.buttonAddRefresh);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "DynamicsGPFormPaystubMain";
            this.Text = "Employee\'s Paystub Sent by Email";
            groupBoxSelectBy.ResumeLayout(false);
            groupBoxSelectBy.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal System.Windows.Forms.Button buttonLookupPayrollDateFrom;
        protected internal System.Windows.Forms.Button buttonLookupPayrollDateTo;
        private System.Windows.Forms.Label labelPayrollFrom;
        protected internal System.Windows.Forms.TextBox textBoxPayrollDateFrom;
        protected internal System.Windows.Forms.TextBox textBoxPayrollDateTo;
        private System.Windows.Forms.Label labelPayrollTo;
        protected internal System.Windows.Forms.TextBox textboxEmployeeIdFrom;
        protected internal System.Windows.Forms.Button btnAccept;
        protected internal System.Windows.Forms.Button buttonGenerateSend;
        protected internal System.Windows.Forms.Button buttonEliminateAll;
        protected internal System.Windows.Forms.Button buttonEliminate;
        protected internal System.Windows.Forms.Button buttonAddRefresh;
        private System.Windows.Forms.Label labelEmployeeIdFrom;
        protected internal System.Windows.Forms.Button buttonLookupEmployeeIdFrom;
        internal System.Windows.Forms.ProgressBar progressBar1;
        protected internal System.Windows.Forms.Button buttonCallConfiguration;
        protected internal System.Windows.Forms.Button buttonWorkflowSetup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonEmployeeId;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.Label labelAuditCode;
        protected internal System.Windows.Forms.TextBox textboxEmployeeIdTo;
        private System.Windows.Forms.Label labelEmployeeIdTo;
        protected internal System.Windows.Forms.Button buttonLookupEmployeeIdTo;
        public System.Windows.Forms.ListView listViewEmployees;

    }
}

