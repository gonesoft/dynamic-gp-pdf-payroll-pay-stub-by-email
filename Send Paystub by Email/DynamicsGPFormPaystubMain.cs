using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Dexterity.Bridge;
using Microsoft.Dexterity.Applications;
using Microsoft.Dexterity.Shell;
using Microsoft.Dexterity.Applications.DynamicsDictionary;

namespace Send_Paystub_by_Email
{
    public partial class DynamicsGPFormPaystubMain : DexUIForm
    {
        public DynamicsGPFormPaystubMain()
        {
            InitializeComponent();

            //Create listview default configuration
            //columns
            listViewEmployees.Columns.Add("Employee Code");
            listViewEmployees.Columns.Add("Employee Name");
            listViewEmployees.Columns.Add("Payroll Date");
            listViewEmployees.Columns.Add("Payroll Code");
            listViewEmployees.Columns.Add("Employee Email");
            listViewEmployees.Columns.Add("Status");

            //Show details 
            listViewEmployees.View = View.Details;

            
            //Set columns header visible by default
            listViewEmployees.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        //This event handles the call to send the email
        private void buttonGenerateSend_Click(object sender, EventArgs e)
        {
            //Creates a reference to get email config
            HtmlEmailHelper config = new HtmlEmailHelper();

            //open the workflow setup form
            Dynamics.Forms.WfSetup.WorkflowSetup.Open();

            

            //this.Hide();

            //get information data from workflow setup 
            config.HostSmtp = Dynamics.Forms.WfSetup.WorkflowSetup.ServerId;
            config.Port = Dynamics.Forms.WfSetup.WorkflowSetup.ServerPort;
            config.SSL = Dynamics.Forms.WfSetup.WorkflowSetup.IsSsl;
            config.EmailSender = Dynamics.Forms.WfSetup.WorkflowSetup.SmtpUser;
            config.EmailPassword = Dynamics.Forms.WfSetup.WorkflowSetup.LocalConfirmPassword;
            Dynamics.Forms.WfSetup.WorkflowSetup.Close();

            //Dynamics.Forms.WfSetup.WorkflowSetup.

            //loop through each listview item to process and send email
            foreach (ListViewItem l in listViewEmployees.Items)
            {
                listViewEmployees.Items[l.Index].SubItems[5].Text = "Message sent";
                /*listViewEmployees.Items[l.Index].SubItems[5].Text = config.GenerateAndSend(config,
                    l.SubItems[3].Text, l.SubItems[0].Text, l.SubItems[4].Text); 
                */

            }
        }
        

        //This event handles the call to workflow setup (email and server configuration)
        private void buttonWorkflowSetup_Click(object sender, EventArgs e)
        {
            //Call and open workflow setup
            Dynamics.Forms.WfSetup.WorkflowSetup.Open();
            
        }

        //This event handles the selection radio button
        private void radioButtonEmployeeId_CheckedChanged(object sender, EventArgs e)
        {
            //Enable or disable search id employee search boxes if employee id radio button is checked
            //if all checked means all employees will be selected for search
            if (radioButtonEmployeeId.Checked)
            {
                textboxEmployeeIdFrom.Text = "";
                textboxEmployeeIdTo.Text = "";
                textboxEmployeeIdFrom.Enabled = true;
                textboxEmployeeIdTo.Enabled = true;
                buttonLookupEmployeeIdFrom.Enabled = true;
                buttonLookupEmployeeIdTo.Enabled = true;
                textboxEmployeeIdFrom.Focus();

            }
            else
            {
                textboxEmployeeIdFrom.Text = "";
                textboxEmployeeIdTo.Text = "";
                textboxEmployeeIdFrom.Enabled = false;
                textboxEmployeeIdTo.Enabled = false;
                buttonLookupEmployeeIdFrom.Enabled = false;
                buttonLookupEmployeeIdTo.Enabled = false;
            }

        }

        //This event handles eliminate one or more row from listview
        private void buttonEliminate_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem l in listViewEmployees.CheckedItems)
            {
                listViewEmployees.Items.Remove(l);
                
     
            }
        }

        //This event handles clear all from listview
        private void buttonEliminateAll_Click(object sender, EventArgs e)
        {
            //Clear all in the listview
            listViewEmployees.Items.Clear();

            //Clear textboxes
            textBoxPayrollDateFrom.Text = "";
            textBoxPayrollDateTo.Text = "";
            textboxEmployeeIdFrom.Text = "";
            textboxEmployeeIdTo.Text = "";

            //set the focus at the begining of the textbox
            textBoxPayrollDateFrom.Focus();
        }

        //This event handles listview fill
        public void buttonAddRefresh_Click(object sender, EventArgs e)
        {
            //Clear up listview
            listViewEmployees.Items.Clear();
            
            //this automatically filters wich method use before look the data
            if (textBoxPayrollDateFrom.Text != "" && textboxEmployeeIdFrom.Text != "")
            {
                if (textboxEmployeeIdFrom.Text == textboxEmployeeIdTo.Text)
                {
                    //search by payroll code range but same employ id
                    DataAccess.GetAllEmployeesByAuditCodeRangeAndEmployeeId(textBoxPayrollDateFrom.Text, textBoxPayrollDateTo.Text, textboxEmployeeIdFrom.Text);
                }
                else
                {
                    //search by Employee id range
                    DataAccess.GetAllEmployeesByEmployeeIdRange(textboxEmployeeIdFrom.Text, textboxEmployeeIdTo.Text);
                }
            }
            else
            {
                //search by payroll code range only, all employee
                DataAccess.GetAllEmployeesByAuditCodeRange(textBoxPayrollDateFrom.Text, textBoxPayrollDateTo.Text);
            }
            
            //Reorder listview
            listViewEmployees.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            listViewEmployees.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

        }

        private void buttonLookupPayrollDateFrom_Click(object sender, EventArgs e)
        {
            //open auditcode lookup window
            GPAddIn.auditcodeLookupObject.Open();

            //indicates that auditcode lookup window is open
            GPAddIn.isPayrollAuditCodeOpen = true;
            GPAddIn.auditcodeLookupObject.UprAuditControlCodeLookup.PbRefreshScrollingWindow.RunValidate();

            //indicates that information selected is going into 'from' textbox
            GPAddIn.isTxtFromFlag = true;

            
        }

        private void buttonLookupPayrollDateTo_Click(object sender, EventArgs e)
        {
            //open auditcode lookup window
            GPAddIn.auditcodeLookupObject.Open();

            //indicates that auditcode lookup window is open
            GPAddIn.isPayrollAuditCodeOpen = true;
            GPAddIn.auditcodeLookupObject.UprAuditControlCodeLookup.PbRefreshScrollingWindow.RunValidate();

            //indicates that information selected is going into 'to' textbox
            GPAddIn.isTxtFromFlag = false;


            
        }

        private void buttonLookupEmployeeIdFrom_Click(object sender, EventArgs e)
        {
            //open the lookup window
            GPAddIn.employeeLookupObject.Open();

            //indicates that lookup windows is open
            GPAddIn.isEmployeeLookupOpen = true;
            GPAddIn.employeeLookupObject.Commands.Refresh.Run();

            //indicates that information selected is going into 'from' textbox
            GPAddIn.isTxtFromFlag = true;

        }

        private void buttonLookupEmployeeIdTo_Click(object sender, EventArgs e)
        {
            //open the lookup window
            GPAddIn.employeeLookupObject.Open();

            //indicates that lookup windows is open
            GPAddIn.isEmployeeLookupOpen = true;
            GPAddIn.employeeLookupObject.Commands.Refresh.Run();

            //indicates that information selected is going into to textbox
            GPAddIn.isTxtFromFlag = false;

            //replace me with search and fill listbox
            //textboxEmployeeIdTo.Focus();
            
        }

        
    }
}